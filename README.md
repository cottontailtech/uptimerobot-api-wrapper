# UptimeRobot API Wrapper

## Introduction

Welcome to UptimeRobotApiWrapper! This is a Node.js module designed to provide a convenient wrapper around the UptimeRobot API, allowing users to easily interact with UptimeRobot's monitoring services in their Node.js applications.

## Installation

You can install UptimeRobotApiWrapper via npm by running the following command in your terminal:

```bash
npm install @cotton-tail-tech/uptimerobot-api-wrapper
```

## Usage

To use UptimeRobotApiWrapper in your Node.js project, you'll first need to obtain an API key from UptimeRobot. Once you have your API key, you can instantiate the UptimeRobotApiWrapper with it and start making requests to the UptimeRobot API.

Here's a basic example of how to use UptimeRobotApiWrapper:

```javascript
const UptimeRobotApiWrapper = require("@cotton-tail-tech/uptimerobot-api-wrapper");

// Instantiate the wrapper with your API key
const apiKey = "your-api-key";
const uptimeRobot = new UptimeRobotApiWrapper(apiKey);

// Example: Get all monitors
uptimeRobot
  .getMonitors()
  .then((monitors) => {
    console.log("Monitors:", monitors);
  })
  .catch((error) => {
    console.error("Error:", error);
  });
```

## Features

- **Get Monitors**: Retrieve a list of monitors associated with your UptimeRobot account.
- **Get Monitor Details**: Get detailed information about a specific monitor.
- **Create Monitor**: Create a new monitor.
- **Delete Monitor**: Delete an existing monitor.

## Contribution

Contributions are welcome! If you find any bugs or have suggestions for new features, please feel free to open an issue or submit a pull request on GitHub.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Disclaimer

UptimeRobotApiWrapper is not officially affiliated with UptimeRobot. It is an independent project developed and maintained by the community.

## Implemented Methods

| Method | Endpoint           | Implemented |
| ------ | ------------------ | ----------- |
| POST   | getAccountDetails  | ❌          |
| POST   | getMonitors        | ❌          |
| POST   | newMonitor         | ❌          |
| POST   | editMonitor        | ❌          |
| POST   | deleteMonitor      | ❌          |
| POST   | resetMonitor       | ❌          |
| POST   | getAlertContacts   | ❌          |
| POST   | newAlertContact    | ❌          |
| POST   | editAlertContact   | ❌          |
| POST   | deleteAlertContact | ❌          |
| POST   | getMWindows        | ❌          |
| POST   | newMWindow         | ❌          |
| POST   | editMWindow        | ❌          |
| POST   | deleteMWindow      | ❌          |
| POST   | getPSPs            | ❌          |
| POST   | newPSP             | ❌          |
| POST   | editPSP            | ❌          |
| POST   | deletePSP          | ❌          |
